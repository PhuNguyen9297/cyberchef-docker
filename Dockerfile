FROM public.ecr.aws/nginx/nginx-unprivileged:1.27-bookworm
COPY CyberChef/build/prod/ /usr/share/nginx/html
EXPOSE 8080
